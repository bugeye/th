# BugEye TH

An incarnation of the [BugEye spirit](https://gitlab.com/bugeye/spirit), **BugEye TH** is a unit testing framework for PHP 7.2+.


## Installation

```batchfile
composer require bugeye/th
```


## Component stability statuses

| Component                                                                                       | Stability | Since |
|:------------------------------------------------------------------------------------------------|:---------:|:-----:|
| [AggregateTest](src/AggregateTest.api.md)                                                       | alpha     | 0.2   |
| [FailException](src/FailException.api.md)                                                       | alpha     | 0.0   |
| [TestInfo](src/TestInfo.api.md)                                                                 | alpha     | 0.0   |
| [TestReport](src/TestReport.api.md)                                                             | alpha     | 0.0   |
| [TestSuiteReport](src/TestSuiteReport.api.md)                                                   | alpha     | 0.2   |
| [TestSuiteRunner](src/TestSuiteRunner.api.md)                                                   | alpha     | 0.2   |
| [Aggregates/ObjectReflectionAggregateTest](src/Aggregates/ObjectReflectionAggregateTest.api.md) | alpha     | 0.2   |
| [Reports/HtmlTestReport](src/Reports/HtmlTestReport.api.md)                                     | alpha     | 0.2   |
| [Reports/HtmlTestSuiteReport](src/Reports/HtmlTestSuiteReport.api.md)                           | alpha     | 0.2   |
| [Reports/PlainTextTestReport](src/Reports/PlainTextTestReport.api.md)                           | alpha     | 0.2   |
| [Reports/PlainTextTestSuiteReport](src/Reports/PlainTextTestSuiteReport.api.md)                 | alpha     | 0.2   |
| [Reports/TestReportAsserts](src/Reports/TestReportAsserts.api.md)                               | alpha     | 0.2   |
| [Reports/TestReportWrapper](src/Reports/TestReportWrapper.api.md)                               | alpha     | 0.2   |
| [Reports/TestSuiteReportHelper](src/Reports/TestSuiteReportHelper.api.md)                       | alpha     | 0.2   |
| [Reports/Utils](src/Reports/Utils.api.md)                                                       | alpha     | 0.2   |
| [Runners/DefaultTestSuiteRunner](src/Runners/DefaultTestSuiteRunner.api.md)                     | alpha     | 0.2   |

The **Stability** column indicates the component's stability status, and the **Since** column indicates the package's major version when the component first achieved that stability.

Each component and its members has a **stability status** indicating how stable the interface and implementation is to depend on in production. The stability may be one of the following:

* **alpha**: The interface and implementation are unstable and may change significantly.
* **beta**: The interface is stable but its implementation is not sufficiently tested.
* **omega**: The interface and implementation are stable and considered ready for production use.

A component's stability status is the same as the highest stability status of its members. Once a member's stability is raised, it will not be reduced.


## License

BugEye TH is licensed under the MIT license. See the [LICENSE](LICENSE.md) file for more information.