# BugEye\TH\AggregateTest


## AggregateTest interface

_Stability: **alpha**, Since: **0.2**_

```php
interface AggregateTest
```


### Methods


#### hasSetup()

```php
function hasSetup() : bool
```


#### hasTeardown()

```php
function hasTeardown() : bool
```


#### getTests()

```php
function getTests() : array<TestInfo>
```


#### getAggregateTest()

```php
function getAggregateTest(string $name) : any
```


#### invokeSetup()

```php
function invokeSetup(TestReport $report) : void
```


#### invokeTeardown()

```php
function invokeTeardown(TestReport $report) : void
```


#### invokeLeafTest()

```php
function invokeLeafTest(string $name, TestReport $report) : void
```