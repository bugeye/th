<?php namespace BugEye\TH;


interface AggregateTest {
	function hasSetup() : bool;


	function hasTeardown() : bool;


	function getTests() : array;


	function getAggregateTest(string $name);


	function invokeSetup(TestReport $report) : void;


	function invokeTeardown(TestReport $report) : void;


	function invokeLeafTest(string $name, TestReport $report) : void;
}