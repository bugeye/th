<?php namespace BugEye\TH\Aggregates;

use \BugEye\TH\AggregateTest;
use \BugEye\TH\TestReport;


class ObjectReflectionAggregateTest implements AggregateTest {
	private $aggregate;
	private $hasSetup;
	private $hasTeardown;
	private $tests;
	private $aggregates = [];


	public function __construct($aggregate) {
		$this->aggregate = $aggregate;

		// TODO: Use $class. (jc)
		$this->hasSetup = method_exists($this->aggregate, 'setup');
		$this->hasTeardown = method_exists($this->aggregate, 'teardown');

		$tests = [];

		$class = new \ReflectionClass($this->aggregate);
		foreach ($class->getMethods() as $method) {
			$methodName = $method->getName();

			if (substr($methodName, 0, 5) === 'test_') {
				$name = substr($methodName, 5);
				$type = 'leaf';
				$status = 'enabled';
				$reason = '';

				$params = $method->getNumberOfParameters();
				if ($params !== 1) {
					if (!$params) {
						$type = 'aggregate';
						$object = $this->aggregate->$methodName();
						$this->aggregates[$name] = $object;
					}
					else {
						throw new \Exception("The method $methodName as too many parameters");
					}
				}

				if (!$method->isPublic()) {
					$status = 'disabled';
					$reason = 'not-public';
				}

				$tests[$name] = [
					'name' => $name,
					'title' => $name,
					'description' => '',
					'type' => $type,
					'status' => $status,
					'reason' => $reason,
				];
			}
		}

		$this->tests = $tests;
		ksort($this->tests);
	}


	public function aggregate() {
		return $this->aggregate;
	}


	//
	// AggregateTest Implementation
	//


	public function hasSetup() : bool {
		return $this->hasSetup;
	}


	public function hasTeardown() : bool {
		return $this->hasTeardown;
	}


	public function getTests() : array {
		return $this->tests;
	}


	public function getAggregateTest(string $name) {
		$info = $this->tests[$name];
		if (!$info)
			throw new \InvalidArgumentException('not a test');

		if ($info['type'] !== 'aggregate')
			throw new \InvalidArgumentException('not an aggregate');

		return $this->aggregates[$name];
	}


	public function invokeSetup(TestReport $report) : void {
		if (!$this->hasSetup)
			throw new \InvalidArgumentException('no setup');

		$this->aggregate->setup($report);
	}


	public function invokeTeardown(TestReport $report) : void {
		if (!$this->hasTeardown)
			throw new \InvalidArgumentException('no teardown');

		$this->aggregate->teardown($report);
	}


	public function invokeLeafTest(string $name, TestReport $report) : void {
		$info = $this->tests[$name];
		if (!$info)
			throw new \InvalidArgumentException('not a test');

		if ($info['type'] !== 'leaf')
			throw new \InvalidArgumentException('not a leaf');

		$methodName = 'test_' . $name;
		$this->aggregate->$methodName($report);
	}
}