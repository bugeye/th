# BugEye\TH\FailException


## FailException class

_Stability: **alpha**, Since: **0.0**_

```php
class FailException extends \Exception
```

A type of [`Exception`](https://www.php.net/manual/en/class.exception.php) that is thrown during a leaf test to halt the test and indicate that it failed.

This exception will typically be thrown by a [`TestReport`](TestReport.api.md)'s `check()` method.


### Constructor


#### __construct()

```php
public function __construct()
```