<?php namespace BugEye\TH\Reports;

use \BugEye\TH\FailException;
use \BugEye\TH\TestReport;


class HtmlTestReport implements TestReport {
	private $written = false;
	private $failed = 0;


	public function __construct(array $config = []) {
		// TODO: Read config. (jc)
	}


	//
	// TestReport implementation
	//


	public function log(...$args) : void {
		$str = true;
		$args2 = [];
		foreach ($args as $arg) {
			if ($str) {
				if ($arg !== null && $arg !== '') {
					if ($arg === false) {
						$arg = 'false';
					}
					else if ($arg === true) {
						$arg = 'true';
					}
					else if (is_string($arg)) {
						$arg = htmlspecialchars($arg);
					}
					else if (is_int($arg) || is_float($arg)) {
						$arg = "$arg";
					}
					else if (is_resource($arg)) {
						$arg = 'res:' . get_resource_type($arg);
					}
					else if (is_array($arg)) {
						$len = count($arg);
						$arg = "[$len]";
					}
					else if (is_object($arg)) {
						if (!method_exists($arg, '__toString')) {
							$cls = get_class($arg);
							$arg = "\{$cls\}";
						}
						else {
							$arg = htmlspecialchars(strval($arg));
						}
					}
					else {
						$arg = 'UNKNOWN';
					}
				}

				if ($arg !== null && $arg !== '')
					$args2[] = "<span>$arg</span>";
			}
			else {
				if ($arg === null) {
					$arg = 'null';
				}
				else if ($arg === false) {
					$arg = 'false';
				}
				else if ($arg === true) {
					$arg = 'true';
				}
				else if (is_string($arg)) {
					$arg = '"' . htmlspecialchars($arg) . '"';
				}
				else if (is_int($arg) || is_float($arg)) {
					$arg = "$arg";
				}
				else if (is_resource($arg)) {
					$arg = 'res:' . get_resource_type($arg);
				}
				else if (is_array($arg)) {
					$len = count($arg);
					$arg = "[$len]";
				}
				else if (is_object($arg)) {
					if (!method_exists($arg, '__toString')) {
						$cls = get_class($arg);
						$arg = "\{$cls\}";
					}
					else {
						$arg = htmlspecialchars(strval($arg));
					}
				}
				else {
					$arg = 'UNKNOWN';
				}

				$args2[] = "<span>$arg</span>";
			}

			$str = !$str;
		}

		if (count($args2))
			echo "<div class='_log'>" . implode(' ', $args2) . "</div>\n";
	}


	public function assert($label, $pass, ...$args) : void {
		echo ($pass ? "<div class='_assert _passed'>\n" : "<div class='_assert _failed'>\n");

		if (!$pass)
			$this->failed++;

		if ($label !== null && $label !== "")
			echo "<div class='_label'>" . htmlspecialchars($label) . "</div>\n";

		$this->log(...$args);

		echo ($pass ? "<div class='_status _passed'></div>\n" : "<div class='_status _failed'></div>\n");
		echo "</div>\n";
	}


	public function check($ex = null) : void {
		if ($this->failed) {
			if ($ex instanceof FailException)
				throw $ex;
			throw new FailException();
		}
	}
}