<?php namespace BugEye\TH\Reports;

use \BugEye\TH\TestReport;
use \BugEye\TH\TestSuiteReport;


class HtmlTestSuiteReport implements TestSuiteReport {
	private $helper;
	private $totalTests;
	private $passedTests;
	private $failedTests;
	private $currTest;


	public function __construct(array $config = []) {
		$this->helper = new TestSuiteReportHelper();
		// TODO: Read config. (jc)
	}


	//
	// TestSuiteReport implementation
	//


	public function beginTestSuite(array $suiteInfo) : void {
		$this->helper->assertBeforeTestSuite();

		$this->totalTests = $suiteInfo['totalTests'];
		$this->passedTests = 0;
		$this->failedTests = 0;
		$this->currTest = 0;

		echo "<div class='bugeye_th_Html'>\n";

		$this->helper->pushContainerTestSuite();
	}


	public function endTestSuite() : void {
		$this->helper->assertInTestSuite();

		$skippedTests = $this->totalTests - $this->passedTests - $this->failedTests;
		echo "<div class='_summary'>\n";
		echo "<div class='_total'>{$this->totalTests}</div>\n";
		echo "<div class='_passed'>{$this->passedTests}</div>\n";
		echo "<div class='_failed'>{$this->failedTests}</div>\n";
		echo "<div class='_skipped'>{$this->skippedTests}</div>\n";
		echo "</div>\n";

		echo "</div>\n";

		$this->helper->popContainer();
	}


	public function beginAggregateTest(array $testInfo) : void {
		$this->helper->assertInAggregate();

		echo "<div class='_aggregate'>\n";

		$this->helper->pushContainerAggregate();
	}


	public function endAggregateTest() : void {
		$this->helper->assertInAggregate();

		echo "</div>\n";

		$this->helper->popContainer();
	}


	public function beingSetupRoutine() : TestReport {
		$this->helper->assertInAggregate();

		$report = new HtmlTestReport();

		echo "<div class='_setup'>\n";

		$this->helper->pushContainerSetup();
		return $report;
	}


	public function endSetupRoutine() : void {
		$this->helper->assertInSetup();

		echo "</div>\n";

		$this->helper->popContainer();
	}


	public function beginTeardownRoutine() : TestReport {
		$this->helper->assertInAggregate();

		$report = new HtmlTestReport();

		echo "<div class='_teardown'>\n";

		$this->helper->pushContainerTeardown();
		return $report;
	}


	public function endTeardownRoutine() : void {
		$this->helper->assertInTeardown();

		echo "</div>\n";

		$this->helper->popContainer();
	}


	public function beginLeafTest(array $testInfo) : TestReport {
		$this->helper->assertInAggregate();

		echo "<div class='_leaf'>\n";
		echo "<div class='_numbers'><span class='_current'>$currTest</span> <span class='_total'>$totalTests</span></div>\n";
		echo "<div class='_name'>$shortName</div>\n";
		echo "<div class='_output'>\n";

		$report = new HtmlTestReport();

		$this->helper->pushContainerLeaf();
		return $report;
	}


	public function endLeafTest($status) : void {
		$this->helper->assertInLeaf();

		if ($status === 'passed') {
			$this->passedTests++;

			echo "</div>\n";
			echo "<div class='_status _passed'></div>\n";
		}
		else if ($status === 'failed') {
			$this->failedTests++;

			echo "</div>\n";
			echo "<div class='_status _failed'></div>\n";
		}
		else {
			// TODO
		}

		echo "</div>\n";

		$this->helper->popContainer();
	}
}