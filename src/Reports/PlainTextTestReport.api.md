# BugEye\TH\PlainTextTestReport


## PlainTextTestReport class

_Stability: **alpha**, Since: **0.2**_

```php
class PlainTextTestReport implements TestReport
```

An implementation of [`TestReport`](TestReport.api.md) that outputs the report of a single test to stdout, formatted as plain text.


### Constructor


#### __construct()

```php
public function __construct(array $config = [])
```


### Methods


#### log()

```php
public function log(...$args) : void
```


#### assert()

```php
public function assert($label, $pass, ...$args) : void
```


#### check()

```php
public function check($ex = null) : void
```