<?php namespace BugEye\TH\Reports;

use \BugEye\TH\FailException;
use \BugEye\TH\TestReport;


class PlainTextTestReport implements TestReport {
	private $written = false;
	private $failed = 0;


	public function __construct(array $config = []) {
		// TODO: Read config. (jc)
	}


	//
	// TestReport implementation
	//


	public function log(...$args) : void {
		$args2 = [];
		foreach ($args as $arg) {
			if ($arg !== null)
				$arg = TestReportHelper::shortString($arg);
			if ($arg !== null && $arg !== '')
				$args2[] = $arg;
		}

		if (count($args2)) {
			if ($this->written)
				echo "\n";
			$this->written = true;
			echo implode(' ', $args2) . "\n";
		}
	}


	public function assert($label, $pass, ...$args) : void {
		if (!$pass)
			$this->failed++;

		if ($this->written)
			echo "\n";
		$this->written = true;

		if ($label !== null && $label !== "")
			echo "$label\n";

		$this->log(...$args);

		echo ($pass ? "assert passed\n" : "assert failed\n");
	}


	public function check($ex = null) : void {
		if ($this->failed) {
			if ($ex instanceof FailException)
				throw $ex;
			throw new FailException();
		}
	}
}