<?php namespace BugEye\TH\Reports;

use \BugEye\TH\TestReport;
use \BugEye\TH\TestSuiteReport;


class PlainTextTestSuiteReport implements TestSuiteReport {
	private $helper;
	private $totalTests;
	private $passedTests;
	private $failedTests;
	private $currTest;


	public function __construct(array $config = []) {
		$this->helper = new TestSuiteReportHelper();
		// TODO: Read config. (jc)
	}


	//
	// TestSuiteReport implementation
	//


	public function beginTestSuite(array $suiteInfo) : void {
		$this->helper->assertBeforeTestSuite();

		$this->totalTests = $suiteInfo['totalTests'];
		$this->passedTests = 0;
		$this->failedTests = 0;
		$this->currTest = 0;

		$this->helper->pushContainerTestSuite();
	}


	public function endTestSuite() : void {
		$this->helper->assertInTestSuite();

		$skippedTests = $this->totalTests - $this->passedTests - $this->failedTests;
		echo "TOTAL TESTS:   {$this->totalTests}\n";
		echo "PASSED TESTS:  {$this->passedTests}\n";
		echo "FAILED TESTS:  {$this->failedTests}\n";
		echo "SKIPPED TESTS: {$skippedTests}\n";

		$this->helper->popContainer();
	}


	public function beginAggregateTest(array $testInfo) : void {
		$this->helper->assertInAggregate();

		$this->helper->pushContainerAggregate();
	}


	public function endAggregateTest() : void {
		$this->helper->assertInAggregate();

		$this->helper->popContainer();
	}


	public function beginSetupRoutine() : TestReport {
		$this->helper->assertInAggregate();

		$report = new PlainTextTestReport();

		$this->helper->pushContainerSetup();
		return $report;
	}


	public function endSetupRoutine() : void {
		$this->helper->assertInSetup();

		$this->helper->popContainer();
	}


	public function beginTeardownRoutine() : TestReport {
		$this->helper->assertInAggregate();

		$report = new PlainTextTestReport();

		$this->helper->pushContainerTeardown();
		return $report;
	}


	public function endTeardownRoutine() : void {
		$this->helper->assertInTeardown();

		$this->helper->popContainer();
	}


	public function beginLeafTest(array $testInfo) : TestReport {
		$this->helper->assertInAggregate();

		$name = $testInfo['name'];
		echo "TEST {$this->currTest} OF {$this->totalTests}: {$name}\n";

		$report = new PlainTextTestReport();

		$this->helper->pushContainerLeaf();
		return $report;
	}


	public function endLeafTest(string $status, string $reason) : void {
		$this->helper->assertInLeaf();

		if ($status === 'passed') {
			$this->passedTests++;
			echo "TEST PASSED\n";
		}
		else if ($status === 'failed') {
			$this->failedTests++;
		}
		else {
			// TODO
		}

		echo "----------\n";

		$this->helper->popContainer();
	}
}