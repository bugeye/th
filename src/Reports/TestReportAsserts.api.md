# BugEye\TH\Reports\TestReportAsserts


## TestReportAsserts class

_Stability: **alpha**, Since: **0.2**_

```php
class TestReportAsserts extends TestReportWrapper
```

A subclass of [`TestReportWrapper`](TestReportWrapper.api.md) that provides a set of utility assert methods.


### Methods


#### op()

```php
public function op($label, $pass, $a, $op, $b)
```


#### truish()

```php
public function truish($label, $a)
```


#### falsish()

```php
public function falsish($label, $a)
```


#### equal()

```php
public function equal($label, $a, $b)
```


#### equalish()

```php
public function equalish($label, $a, $b)
```


#### notEqual()

```php
public function notEqual($label, $a, $b)
```


#### notEqualish()

```php
public function notEqualish($label, $a, $b)
```


#### greaterThan()

```php
public function greaterThan($label, $a, $b)
```


#### greaterThanOrEqualish()

```php
public function greaterThanOrEqual($label, $a, $b)
```


#### lessThan()

```php
public function lessThan($label, $a, $b)
```


#### lessThanOrEqualish()

```php
public function lessThanOrEqual($label, $a, $b)
```


#### instanceOf()

```php
public function instanceOf($label, $a, $b)
```


#### matches()

```php
public function matches($label, $a, $b)
```


#### typeOf()

```php
public function typeOf($label, $a, $b)
```