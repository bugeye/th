<?php namespace BugEye\TH\Reports;

use \BugEye\TH\TestReport;


class TestReportAsserts extends TestReportWrapper {
	public function __construct(TestReport $wrapped) {
		parent::__construct($wrapped);
	}


	public function op($label, $pass, $a, $op, $b) {
		$this->assert($label, $pass, null, $a, $op, $b);
	}


	public function truish($label, $a) {
		$this->op($label, $a == true, $a, "==", true);
	}


	public function falsish($label, $a) {
		$this->op($label, $a == false, $a, "==", false);
	}


	public function equal($label, $a, $b) {
		$this->op($label, $a === $b, $a, "===", $b);
	}


	public function equalish($label, $a, $b) {
		$this->op($label, $a == $b, $a, "==", $b);
	}


	public function notEqual($label, $a, $b) {
		$this->op($label, $a !== $b, $a, "!==", $b);
	}


	public function notEqualish($label, $a, $b) {
		$this->op($label, $a != $b, $a, "!=", $b);
	}


	public function greaterThan($label, $a, $b) {
		$this->op($label, $a > $b, $a, ">", $b);
	}


	public function greaterThanOrEqual($label, $a, $b) {
		$this->op($label, $a >= $b, $a, ">=", $b);
	}


	public function lessThan($label, $a, $b) {
		$this->op($label, $a < b, $a, "<", $b);
	}


	public function lessThanOrEqual($label, $a, $b) {
		$this->op($label, $a <= $b, $a, "<=", $b);
	}


	public function instanceOf($label, $a, $b) {
		$this->op($label, $a instanceof $b, $a, "instanceof", $b);
	}


	public function matches($label, $a, $b) {
		$this->assert($label, $b.test($a), "", $b, ".test(", $a, ")");
	}


	public function typeOf($label, $a, $b) {
		$this->assert($label, gettype($a) === $b, "gettype(", $a, ") ===", $b);
	}
}