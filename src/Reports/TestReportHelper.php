<?php namespace BugEye\TH\Reports;


class TestReportHelper {
	private function __construct() {
		// Do nothing.
	}


	static public function shortString($v) {
		if ($v === null) {
			return 'null';
		}
		else if ($v === false) {
			return 'false';
		}
		else if ($v === true) {
			return 'true';
		}
		else if (is_string($v)) {
			return '"' . addslashes($v) . '"';
		}
		else if (is_int($v) || is_float($v)) {
			return "$v";
		}
		else if (is_resource($v)) {
			return 'res:' . get_resource_type($v);
		}
		else if (is_array($v)) {
			$len = count($v);
			return "[$len]";
		}
		else if (is_object($v)) {
			if (!method_exists($v, '__toString')) {
				$cls = get_class($v);
				return "\{$cls\}";
			}
			else {
				return '$' . strval($v);
			}
		}
		else {
			return 'UNKNOWN';
		}
	}
}