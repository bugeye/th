# BugEye\TH\Reports\TestReportWrapper


## TestReportWrapper class

_Stability: **alpha**, Since: **0.2**_

```php
class TestReportWrapper implements TestReport
```

An implementation of [`TestReport`](TestReport.api.md) that facilitates wrapping another instance of `TestReport`.


### Constructor


#### __construct()

```php
public function __construct(TestReport $wrapped)
```


### Methods


#### wrapped()

```php
public function wrapped()
```


#### log()

```php
function log(...$args) : void
```


#### assert()

```php
function assert($label, $pass, ...$args) : void
```


#### check()

```php
function check($ex = null) : void
```