<?php namespace BugEye\TH\Reports;

use \BugEye\TH\TestReport;


class TestReportWrapper implements TestReport {
	protected $wrapped;


	public function __construct(TestReport $wrapped) {
		$this->wrapped = $wrapped;
	}


	public function wrapped() {
		return $this->wrapped;
	}


	//
	// TestReport Implementation
	//


	public function log(...$args) : void {
		$this->wrapped->log(...$args);
	}


	public function assert($label, $pass, ...$args) : void {
		$this->wrapped->assert($label, $pass, ...$args);
	}


	public function check($ex = null) : void {
		$this->wrapped->check($ex);
	}
}