<?php namespace BugEye\TH\Reports;


class TestSuiteReportHelper {
	private $container = null;
	private $containerStack = [];


	public function assertBeforeTestSuite() {
		if ($this->container !== null)
			throw new \Exception('bad state');
	}


	public function assertInTestSuite() {
		if ($this->container !== 'test-suite')
			throw new \Exception('bad state');
	}


	public function assertInAggregate() {
		if ($this->container !== 'test-suite' && $this->container !== 'aggregate')
			throw new \Exception('bad state');
	}


	public function assertInSetup() {
		if ($this->container !== 'setup')
			throw new \Exception('bad state');
	}


	public function assertInTeardown() {
		if ($this->container !== 'teardown')
			throw new \Exception('bad state');
	}


	public function assertInLeaf() {
		if ($this->container !== 'leaf')
			throw new \Exception('bad state');
	}


	public function pushContainerTestSuite() {
		array_push($this->containerStack, $this->container);
		$this->container = 'test-suite';
	}


	public function pushContainerAggregate() {
		array_push($this->containerStack, $this->container);
		$this->container = 'aggregate';
	}


	public function pushContainerSetup() {
		array_push($this->containerStack, $this->container);
		$this->container = 'setup';
	}


	public function pushContainerTeardown() {
		array_push($this->containerStack, $this->container);
		$this->container = 'teardown';
	}


	public function pushContainerLeaf() {
		array_push($this->containerStack, $this->container);
		$this->container = 'leaf';
	}


	public function popContainer() {
		$this->container = array_pop($this->containerStack);
		if ($this->container === null)
			$this->container = 'end';
	}
}