# BugEye\TH\Runners\DefaultTestSuiteRunner


## DefaultTestSuiteRunner class

_Stability: **alpha**, Since: **0.2**_

```php
class DefaultTestSuiteRunner implements TestSuiteRunner
```