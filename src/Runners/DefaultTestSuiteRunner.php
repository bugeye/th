<?php namespace BugEye\TH\Runners;

use \BugEye\TH\TestSuiteRunner;

use \JCain\Asserts\LR\AssertArg;


class DefaultTestSuiteRunner implements TestSuiteRunner {
	private $testSuiteKind;
	private $testSuiteId;
	private $testSuite;
	private $tests;

	private $instantiateOptions;
	private $constructorParams;

	private $state;


	public function __construct(array $config) {
		$this->createTestSuiteReport = AssertArg::isClosure($config['createTestSuiteReport'], "\$config['createTestSuiteReport']");
	}


	//
	// TestSuiteRunner Implementation
	//


	public function initiate(array $config) : void {
		$this->resetInitiate();

		$this->testSuiteKind = $config['testSuiteKind'];
		$this->testSuiteId = $config['testSuiteId'];

		switch ($this->testSuiteKind) {
			case 'class':
				// Check class existance.

				$classExists = class_exists($this->testSuiteId);
				if (!$classExists) {
					if (interface_exists($this->testSuiteId)) {
						echo "The ID points to an interface type";
					}
					else if (trait_exists($this->testSuiteId)) {
						echo "The ID points to a trait type";
					}
					else {
						echo "The ID does not poing to an existing class";
					}
					break;
				}

				// Check class interface.

				if (!is_a($this->testSuiteId, 'BugEye\\TH\\AggregateTest', true)) {
					echo 'The ID does not point to an implementation of BugEye\\TH\\AggregateTest';
					break;
				}

				// Check instantiationInterface method.

				$methodExists = method_exists($this->testSuiteId, 'instantiationInterface');
				if (!$methodExists) {
					echo 'The instantiationInterface method does not exist';
					break;
				}

				$method = new \ReflectionMethod($this->testSuiteId, 'instantiationInterface');
				if (!$method->isStatic() || !$method->isPublic()) {
					echo 'The instantiationInterface method is either not public or not static';
					break;
				}
				if ($method->getNumberOfParameters()) {
					echo 'The instantiationInterface method must not have any parameters';
					break;
				}

				$methodReturnType = $method->getReturnType();
				if ($methodReturnType !== null && $methodReturnType->getName() !== 'string') {
					echo 'The instantiationInterface method must return a string';
					break;
				}

				$instantiationInterface = $method->invoke(null);
				if (!is_string($instantiationInterface)) {
					echo 'The instantiationInterface method must return a string';
					break;
				}

				if ($instantiationInterface !== 'bugeye-th-constructor') {
					echo 'The instantiationInterface method returned an unsupported value';
					break;
				}

				// Check constructor method.

				$constructorParameter = false;
				$constructorExists = method_exists($this->testSuiteId, '__construct');
				if ($constructorExists) {
					$constructor = new \ReflectionMethod($this->testSuiteId, '__construct');
					if (!$constructor->isPublic()) {
						echo 'The constructor must be public';
						break;
					}

					$this->constructorParams = $constructor->getNumberOfParameters();
					if ($this->constructorParams > 1) {
						echo 'The constructor must either have 0 parameters or 1 that takes an array';
						break;
					}

					// TODO: Check that the first parameter can take an array. (jc)
				}

				// Check instantiateOptions method.

				// See if instantiateOptions() exists, has no parameters, and returns an array.
				// Invoke the method.
				// Check that the structure of the returned value is valid.
				// If inspecting, display options and exit.

				break;

			case 'file':
				// Check file existance.
				// Sniff file type.
				// Parse file.
				break;

			default:
				throw new \InvalidArgumentException("\$config['testSuiteKind'] : Unsupported test suite kind '{$this->testSuiteKind}'");
		}

		$this->state = 'initiated';
	}


	public function getInstantiateOptions() : array {
		$this->checkInitiated();

		return $this->instantiateOptions;
	}


	public function instantiate(array $config) : void {
		$this->checkInitiated();
		$this->resetInstantiate();

		switch ($this->testSuiteKind) {
			case 'class':
				// Instantiate

				// Check user-provided values against possible values.
				// If there are discrepencies, output warnings.

				if ($this->constructorParams) {
					$testSuiteConfig = [];
					$testSuite = new $this->testSuiteId($testSuiteConfig);
				}
				else {
					$testSuite = new $this->testSuiteId();
				}

				// Inspect

				$this->testSuite = $testSuite;
				$this->tests = $this->testSuite->getTests();

				foreach ($this->tests as &$test)
					$test['path'] = $test['name'];

				break;

			case 'file':
				throw new \Exception('not implemented');

			default:
				throw new \LogicException();
		}

		$this->state = 'instantiated';
	}


	public function getTests() : array {
		$this->checkInstantiated();

		return [];
	}


	public function invoke(array $plan) : void {
		$this->checkInstantiated();
		$this->resetInvoke();

		// $plan['orderStrategy'] = 'natural' | 'random-hierarchical' | 'random-flat'
		// $plan['setupStrategy'] = 'grouped' | 'shallow' | 'deep'
		// $plan['tests'] = [ 'leafTest1', 'leftTest2', 'subAggregateA/test1', '...' ];
		$paths = array_keys($this->tests);

		$suiteReport = ($this->createTestSuiteReport)();
		$suiteReport->beginTestSuite([ 'totalTests' => 5 ]);
		$this->invokeAggregateTest($this->testSuite, $suiteReport, '', $paths);
		$suiteReport->endTestSuite();

		$this->state = 'invoked';
	}


	public function getInvokedPlan() : array {
		$this->checkInvoked();

		return [];
	}


	//
	// Utility
	//


	private function resetInitiate() {
		$this->resetInstantiate();
		$this->state = null;
	}


	private function checkInitiated() {
		if ($this->state !== 'initiated' && $this->state !== 'instantiated' && $this->state !== 'invoked')
			throw new \Exception('The test suite has not been initiated');
	}


	private function resetInstantiate() {
		$this->resetInvoke();
		$this->state = 'initiated';
	}


	private function checkInstantiated() {
		if ($this->state !== 'instantiated' && $this->state !== 'invoked')
			throw new \Exception('The test suite has not been instantiated');
	}


	private function resetInvoke() {
		$this->state = 'instantiated';
	}


	private function checkInvoked() {
		if ($this->state !== 'invoked')
			throw new \Exception('The test suite has not been invoked');
	}


	private function invokeAggregateTest($aggregate, $suiteReport, $basePath, $subPaths) {
		if ($aggregate->hasSetup()) {
			$leafReport = $suiteReport->beginSetupRoutine();
			$aggregate->invokeSetup($leafReport);
			$suiteReport->endSetupRoutine();
		}

		foreach ($subPaths as $subPath) {
			$path = "$basePath$subPath";
			//$currTest++;

			$info = $this->tests[$path];
			if ($info['status'] !== 'enabled')
				continue;

			$name = $info['name'];

			if ($info['type'] === 'leaf') {
				$leafReport = $suiteReport->beginLeafTest($info);

				try {
					$result = 'failed';
					$aggregate->invokeLeafTest($name, $leafReport);
					$leafReport->check();
					$result = 'passed';
				}
				catch (\Exception $ex) {
					//if (!($ex instanceof FailException))
					//	echo "$ex\n";
				}

				$suiteReport->endLeafTest($result, '');
			}
			else if ($info['type'] === 'aggregate') {
				$subaggregate = $aggregate->getAggregateTest($name);

				$suiteReport->beginAggregateTest($info);
				$this->invokeAggregateTest($subaggregate, $suiteReport, "$path/", []);
				$suiteReport->endAggregateTest();
			}
			else {
				throw new \LogicException();
			}
		}

		if ($aggregate->hasTeardown()) {
			$leafReport = $suiteReport->beginTeardownRoutine();
			$aggregate->invokeTeardown($leafReport);
			$suiteReport->endTeardownRoutine();
		}
	}
}