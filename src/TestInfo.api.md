# BugEye\TH\TestInfo


## TestInfo array

_Stability: **alpha**, Since: **0.0**_

```php
$testInfo = [
  'name' => 'DoTheSuite',
  'title' => 'Do The Suite',
  'description' => '',
  'type' => 'leaf|aggregate',
  'status' => 'enabled|disabled',
  'reason' => 'not-public',
];
```