# BugEye\TH\TestReport


## TestReport interface

_Stability: **alpha**, Since: **0.0**_

```php
interface TestReport
```

Provides methods for a single leaf test (or setup/teardown routine) to output information to the report.

Leaf tests should not save a reference to the `TestReport` outside the scope of the test.

Implementations should provide a way to indicate that the `TestReport` is invalid after the test for which it was used has concluded. The purpose of this is to prevent confusion in case another call to one of the `TestReport`'s methods happened after the test was done.


### Methods


#### log()

```php
function log(...$args) : void
```

Outputs a line of text. The even numbered indexes of `$args` are converted to strings. The odd numbered indexes are formatted as value details.


#### assert()

```php
function assert($label, $pass, ...$args) : void
```

Outputs the results of an assertion. The `$pass` is a boolish value that indicates whether or not the assertion passed. The `$args` parameter works like it does with `log()`.


#### check()

```php
function check($ex = null) : void
```

Checks if one or more previously called `assert()` methods had failed (i.e. `$pass` was falsish), and if so, throws a [`FailException`](FailException.api.md).