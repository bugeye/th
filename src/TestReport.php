<?php namespace BugEye\TH;


interface TestReport {
	function log(...$args) : void;


	function assert($label, $pass, ...$args) : void;


	function check($ex = null) : void;
}