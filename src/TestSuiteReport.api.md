# BugEye\TH\TestSuiteReport


## TestSuiteReport interface

_Stability: **alpha**, Since: **0.2**_

```php
interface TestSuiteReport
```

Produces the output report of a test suite run.

| Context           | Next                                                                                                      | Description                                                                              |
|:------------------|:----------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------|
| *Ready*           | *TestSuiteA*                                                                                              | The test suite report has not begun.                                                     |
| *TestSuiteA*      | *AggregateTestA* \| *Done*                                                                                | In the test-suite, but the root aggregate has not begun.                                 |
| *TestSuiteB*      | *Done*                                                                                                    | In the test-suite, after the root aggregate has ended.                                   |
| *AggregateTestA*  | *SetupRoutine* \| *AggregateTestA* \| *LeafTest* \| *TeardownRoutine* \| *AggregateTestB* \| *TestSuiteB* | In an aggregate test, before the setup routine, tests, and teardown routine.             |
| *AggregateTestB*  | *AggregateTestA* \| *LeafTest* \| *TeardownRoutine* \| *AggregateTestB* \| *TestSuiteB*                   | In an aggregate test, after the setup routine or tests, but before the teardown routine. |
| *AggregateTestC*  | *AggregateTestB* \| *TestSuiteB*                                                                          | In an aggregate test, after the teardown routine.                                        |
| *LeafTest*        | *AggregateTestB*                                                                                          | In a leaf test.                                                                          |
| *SetupRoutine*    | *AggregateTestB*                                                                                          | In a setup routine.                                                                      |
| *TeardownRoutine* | *AggregateTestC*                                                                                          | In a teardown routine.                                                                   |
| *Done*            | (none)                                                                                                    | The test suite report has ended.                                                         |


### Methods


#### beginTestSuite()

```php
function beginTestSuite(array $suiteInfo) : void
```

Begins the test suite report. Sets the context to *TestSuiteA*.

If the context is not *Ready*, an exception is thrown.


#### endTestSuite()

```php
function endTestSuite() : void
```

Ends the test suite report. Sets the context to *Done*.

If the context is not *TestSuiteA* or *TestSuiteB*, an exception is thrown.


#### beginAggregateTest()

```php
function beginAggregateTest(array $testInfo) : void
```

Begins an aggregate test. Sets the context to *AggregateTestA*.

If the context is not *TestSuiteA*, *AggregateTestA*, or *AggregateTestB*, an exception is thrown.


#### endAggregateTest()

```php
function endAggregateTest() : void
```

Ends an aggregate test. Sets the context to *AggregateTestB* or *TestSuiteB*.

If the context is not *AggregateTestA*, *AggregateTestB*, or *AggregateTestC*, an exception is thrown.


#### beginSetupRoutine()

```php
function beginSetupRoutine() : TestReport
```

Begins an aggregate's setup routine. Sets the context to *SetupRoutine*.

If the context is not *AggregateTestA*, an exception is thrown.


#### endSetupRoutine()

```php
function endSetupRoutine() : void
```

Ends an aggregate's setup routine. Sets the context to *AggregateTestB*.

If the context is not *SetupRoutine*, an exception is thrown.


#### beginTeardownRoutine()

```php
function beginTeardownRoutine() : TestReport
```

Begins an aggregate's teardown routine. Sets the context to *TeardownRoutine*.

If the context is not *AggregateTestA* or *AggregateTestB*, an exception is thrown.


#### endTeardownRoutine()

```php
function endTeardownRoutine() : void
```

Ends an aggregate's teardown routine. Sets the context to *AggregateTestC*.

If the context is not *TeardownRoutine*, an exception is thrown.


#### beginLeafTest()

```php
function beginLeafTest(array $testInfo) : TestReport
```

Begin's an aggregate's leaf test. Sets the context to *LeafTest*.

If the context is not *AggregateTestA* or *AggregateTestB*, an exception is thrown.


#### endLeafTest()

```php
function endLeafTest(string $status, string $reason) : void
```

Ends an aggregate's leaf test. Sets the context to *AggregateTestB*.

If the context is not *LeafTest*, an exception is thrown.