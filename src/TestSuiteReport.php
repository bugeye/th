<?php namespace BugEye\TH;


interface TestSuiteReport {
	function beginTestSuite(array $suiteInfo) : void;


	function endTestSuite() : void;


	function beginAggregateTest(array $testInfo) : void;


	function endAggregateTest() : void;


	function beginSetupRoutine() : TestReport;


	function endSetupRoutine() : void;


	function beginTeardownRoutine() : TestReport;


	function endTeardownRoutine() : void;


	function beginLeafTest(array $testInfo) : TestReport;


	function endLeafTest(string $status, string $reason) : void;
}