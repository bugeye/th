# BugEye\TH\TestSuiteRunner


## TestSuiteRunner interface

_Stability: **alpha**, Since: **0.2**_

```php
interface TestSuiteRunner
```

Loads and runs a test suite to produce a report.


### Methods


#### initiate()

```php
function initiate(array $config) : void
```


#### getInstantiateOptions()

```php
function getInstantiateOptions() : array
```


#### instantiate()

```php
function instantiate(array $config) : void
```


#### getTests()

```php
function getTests() : array
```


#### invoke()

```php
function invoke(array $plan) : void
```

```php
$plan = [
  'orderStrategy' => 'natural',
  'setupStrategy' => 'grouped',
  'tests' => [
    'a/',
    'b/x',
    'b/y',
  ],
]
```


#### getInvokedPlan()

```php
function getInvokedPlan() : array
```

Gets the verbose plan invoked that can be used to recreate the run. The returned array can be passed as an argument to `invoke()`.

The `'orderStrategy'` property is always `'specified'`. The `'tests'` property has every leaf test run explicitly listed.