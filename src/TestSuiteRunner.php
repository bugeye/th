<?php namespace BugEye\TH;


interface TestSuiteRunner {
	function initiate(array $config) : void;


	function getInstantiateOptions() : array;


	function instantiate(array $config) : void;


	function getTests() : array;


	function invoke(array $plan) : void;


	function getInvokedPlan() : array;
}